import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  doLogin(){
    if (this.username === 'admin' && this.password === '123'){
      console.log('login successfull');
      this.router.navigateByUrl('employee');
    }
    else {
      console.error('username or password incorrect');
    }
  }

}
