import {RouterModule, Routes} from '@angular/router';
import {EmployeeComponent} from './employee/employee.component';
import {NgModel} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login/login.component';
import {ChatComponent} from './chat/chat/chat.component';
import {PersononeComponent} from './chat/personone/personone.component';
import {PersontwoComponent} from './chat/persontwo/persontwo.component';


const  routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'employee', component: EmployeeComponent, pathMatch: 'full'},
  {path: 'registration', component: RegistrationComponent, pathMatch: 'full'},
  {path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule)},
  {path: 'chat',  loadChildren: () => import('./chat/chat.module').then(m => m.ChatModule)},
];
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  })
export class AppRoutingModule {}

