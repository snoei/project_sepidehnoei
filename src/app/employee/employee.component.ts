import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {RestClientService} from '../rest-client.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private router: Router, private restClient: RestClientService) { }

  employeeList = [];

  ngOnInit(): void {
    this.restClient.getEmployee().subscribe(res => {
      console.log('employee list', res);
      this.employeeList = res.data;
    });
  }

  navigateToComponentRegistration(){
    this.router.navigateByUrl('/registration');
  }

  navigateToComponentEmployeetwo(){
    this.router.navigateByUrl('/employee');
  }

  navigateToComponentChat_employee(){
    this.router.navigateByUrl('/chat');
  }

  deleteEmployee(id: number){
   this.restClient.deleteEmployee(id).subscribe( res => {
     console.log('delete successful', res);
   });
  }

}
