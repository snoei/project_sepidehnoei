import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Employee} from './employee';
import {RestClientService} from '../rest-client.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private router: Router, private restClient: RestClientService) { }

  employeeList = [];
  employee: Employee;

  ngOnInit() {
    this.employee = {name: '', salary: null, age: null, id: null};
    this.getAllEmployee();
  }

  getAllEmployee(){
    this.restClient.getEmployee().subscribe(res => {
      console.log('employee list', res);
      this.employeeList = res.data;
    });
  }

  navigateToComponentEmployee(){
    this.router.navigateByUrl('/employee');
  }

  navigateToComponentChat_register(){
    this.router.navigateByUrl('/chat');
  }

  saveEmployee(){
   this.restClient.saveEmployee(this.employee).subscribe(res => {
     console.log('server response', res);
   });
  }

}
