import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  constructor(private router: Router) { }
  data = '';

  ngOnInit(): void {
  }

  transLatMessage(event: string){
    this.data = event;
  }

  navigateToComponentEmployee_chat(){
    this.router.navigateByUrl('/employee');
  }

  navigateToComponentChat_chat(){
    this.router.navigateByUrl('/chat/chat');
  }

}
