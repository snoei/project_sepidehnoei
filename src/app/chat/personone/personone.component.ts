import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';

@Component({
  selector: 'app-personone',
  templateUrl: './personone.component.html',
  styleUrls: ['./personone.component.css']
})
export class PersononeComponent implements OnInit {

  message = [];
  constructor() { }
  @Input()
    messageSub1: Subject<any>;

   @Output()
   sendMessage1: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {}

  addtodo(value){
    this.message.push(value);
    console.log(this.message);
    this.sendMessage1.emit(this.message);
  }

}
