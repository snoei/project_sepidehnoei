import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersononeComponent } from './personone.component';

describe('PersononeComponent', () => {
  let component: PersononeComponent;
  let fixture: ComponentFixture<PersononeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersononeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersononeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
