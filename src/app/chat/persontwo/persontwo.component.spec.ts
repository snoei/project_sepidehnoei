import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersontwoComponent } from './persontwo.component';

describe('PersontwoComponent', () => {
  let component: PersontwoComponent;
  let fixture: ComponentFixture<PersontwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersontwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersontwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
