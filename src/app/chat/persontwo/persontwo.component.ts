import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';

@Component({
  selector: 'app-persontwo',
  templateUrl: './persontwo.component.html',
  styleUrls: ['./persontwo.component.css']
})
export class PersontwoComponent implements OnInit {

  message = [];
  constructor() { }

  @Input()
  messageSub2: Subject<any>;

  @Output()
  sendMessage2: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {
  }

  addtodo2(value){
    this.message.push(value);
    console.log(this.message);
    this.sendMessage2.emit(this.message);
  }

}
