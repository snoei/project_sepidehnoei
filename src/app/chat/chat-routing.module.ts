import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ChatComponent} from './chat/chat.component';
import {PersononeComponent} from './personone/personone.component';
import {PersontwoComponent} from './persontwo/persontwo.component';


const routes: Routes = [
  {path: '', component: ChatComponent},
  {path: 'chat', component: ChatComponent},
  {path: 'personone', component: PersononeComponent},
  {path: 'persontwo', component: PersontwoComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
