import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './chat/chat.component';
import { PersononeComponent } from './personone/personone.component';
import { PersontwoComponent } from './persontwo/persontwo.component';
import {ButtonModule} from 'primeng/button';
import {FormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';


@NgModule({
  declarations: [ChatComponent, PersononeComponent, PersontwoComponent],
  exports: [
    ChatComponent,
    PersononeComponent,
    PersontwoComponent,
  ],
  imports: [
    CommonModule,
    ChatRoutingModule,
    FormsModule,
    ButtonModule,
    FormsModule,
    InputTextModule,
  ]
})
export class ChatModule {}
