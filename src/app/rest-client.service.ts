import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {Employee} from './registration/employee';

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  constructor(private http: HttpClient) { }

  getEmployee(): Observable<any> {
     return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }

  saveEmployee(employee: Employee): Observable<any> {
    return this.http.post('http://dummy.restapiexample.com/api/v1/create', employee);
  }

  deleteEmployee(id: number): Observable<any> {
    return this.http.delete('http://dummy.restapiexample.com/api/v1/delete/' + id);
  }
}
